package net.adetech.dc.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import net.adetech.dc.service.DDPushUDPService;

import io.dcloud.PandoraEntry;
import io.dcloud.PandoraEntryActivity;
import io.dcloud.WebAppActivity;

/**
 * Created by zhangsong on 2017/9/25.
 */

public class MainActivity extends Activity {
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = this.getIntent();


        intent.putExtra("is_stream_app", false);

        intent.putExtra("short_cut_class_name", PandoraEntry.class.getName());
        intent.setClass(this, PandoraEntryActivity.class);


        this.startActivity(intent);
        (new Handler()).postDelayed(new Runnable() {
            public void run() {
                MainActivity.this.finish();
            }
        }, 20L);


    }
}
