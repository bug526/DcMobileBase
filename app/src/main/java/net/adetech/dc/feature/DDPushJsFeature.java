package net.adetech.dc.feature;

import android.content.Intent;
import android.util.Log;

import net.adetech.dc.service.DDPushUDPService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.dcloud.common.DHInterface.IWebview;
import io.dcloud.common.DHInterface.StandardFeature;
import io.dcloud.common.util.JSUtil;

/**
 * Created by zhangsong on 2017/9/28.
 */

public class DDPushJsFeature extends StandardFeature {


    public static IWebview WEBVIEW;
    public static String  CALLBACK_ID;

    public void start(IWebview webview, JSONArray jsonArray)
    {

        try {
            WEBVIEW = webview;
            CALLBACK_ID = jsonArray.getString(0);
            String ip = jsonArray.getString(1);
            int port = jsonArray.getInt(2);
            String userId = jsonArray.getString(3);
            Intent intent = new Intent(webview.getActivity(), DDPushUDPService.class);
            intent.putExtra("userId",userId);
            intent.putExtra("ip",ip);
            intent.putExtra("port",port);
            intent.putExtra("action",DDPushUDPService.ACTION_START);


            webview.getActivity().startService(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void stop(IWebview webview,JSONArray jsonArray)
    {
        Intent intent = new Intent(webview.getActivity(), DDPushUDPService.class);
        intent.putExtra("action",DDPushUDPService.ACTION_STOP);
        webview.getActivity().startService(intent);
    }


    public void refresh(IWebview webview,JSONArray jsonArray)
    {
        WEBVIEW = webview;
        try {
            CALLBACK_ID = jsonArray.getString(0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
