package net.adetech.dc.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import net.adetech.dc.activity.MainActivity;
import net.adetech.dc.feature.DDPushJsFeature;

import org.json.JSONException;
import org.json.JSONObject;

import io.dcloud.common.util.JSUtil;

/**
 * Created by zhangsong on 2017/10/3.
 */

public class NotificationClickReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            Intent i = new Intent(context, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(i);
            JSONObject pushInfo = new JSONObject(intent.getStringExtra("info"));
            pushInfo.put("op",2);
            JSUtil.execCallback(DDPushJsFeature.WEBVIEW,DDPushJsFeature.CALLBACK_ID,pushInfo,JSUtil.OK,false);


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
