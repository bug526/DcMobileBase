mui.init({
	swipeBack: true //启用右滑关闭功能
});
mui.plusReady(function() {
	var self = plus.webview.currentWebview();
	var shld = localStorage.getItem("shld");
	var adminuser = JSON.parse(localStorage.adminuser);
	var obj = null;
	weibkfun();
	$(function() {
		if(typeof(myid) != 'undefined') {
			$.get(dataDomain + "/dc/info/" + myid, function(req) {
				obj = req;

				$("#bz").val(req.remark);
				$("#idstr").val(req.idstr);
				setids(req.alarmIds);
			});
		}
		$("#savebkr").on("tap", function() {
			var bz = $("#bz").val();
			var idstr = localStorage.getItem("idstr");
			if(bz == '') {
				mui.alert("请输入备注");
				return false;
			}
			$.ajax({
				type: "POST",
				url: dataDomain + "/dc/info/close",
				data: {
					ids: idstr,
					remark: bz
				},
				async: true,
				success: function(req) {
					if(req.status == 'ok') {
						mui.toast("提交成功");
						var parentWin = plus.webview.getWebviewById('dc');
						parentWin.evalJS("reload()");
						plus.webview.currentWebview().close();
					} else {
						mui.toast("提交失败，请稍候再试");
					}
				}
			});

		});
	});
});

function weibkfun() {
	$.get(dataDomain + "/common/auditusers", function(req) {
		if(req.length > 0) {
			var htm = '';
			for(var i = 0; i < req.length; i++) {
				htm += '<li class="mui-table-view-cell mui-left">';
				htm += '审核人<span style="padding-left:30px;" class="mui-table-view-cell mui-checkbox mui-left"><input style="left:10px;" myid="' + req[i].value + '" type="checkbox" /></span><span myid="' + req[i].value + '" class="view-detail1">' + req[i].text;
				htm += '</span></li>';
			}
			$("#weibk").html(htm);
		}
	})
}