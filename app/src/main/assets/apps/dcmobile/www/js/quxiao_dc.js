mui.init({
	swipeBack: true //启用右滑关闭功能
});
mui.plusReady(function() {
	var self = plus.webview.currentWebview();
	//					var myid = self.mainid;
	var shld = localStorage.getItem("shld");
	var adminuser = JSON.parse(localStorage.adminuser);
	var obj = null;
	weibkfun();
	$(function() {
		if(typeof(myid) != 'undefined') {
			$.get(dataDomain + "/dc/info/" + myid, function(req) {
				obj = req;

				$("#bz").val(req.remark);
				$("#idstr").val(req.idstr);
				setids(req.alarmIds);
			});
		}
		$("#savebkr").on("tap", function() {
			var weibk = $("#weibk").val();
			if(weibk == "") {
				mui.alert("请选择审核人");
				return false;
			}

			var bz = $("#bz").val();
			var idstr = localStorage.getItem("idstr");
			if(bz == '') {
				mui.alert("请输入取消原因");
				return false;
			}
			$.ajax({
				type: "POST",
				url: dataDomain + "/dc/info/applyclose",
				data: {
					ids: idstr,
					remark: bz,
					auditer: weibk
				},
				async: true,
				success: function(req) {
					if(req.status == 'ok') {
						mui.toast("提交成功");
						var parentWin = plus.webview.getWebviewById('dc');
						parentWin.evalJS("reload()");
						plus.webview.currentWebview().close();
					} else {
						mui.toast("提交失败，请稍候再试");
					}
				}
			});

		});
	});
});

function weibkfun() {
	$.get(dataDomain + "/common/auditusers", function(req) {
		if(req.length > 0) {
			var htm = '';
			var weibk = $("#weibk");
			for(var i = 0; i < req.length; i++) {
				htm += '<option value="' + req[i].value + '">';
				htm += req[i].text;
				htm += '</option></li>';
			}
			$("#weibk").html(htm);
		}
	})
}