mui.init({
	swipeBack: true
});
mui('#scroll').scroll({
	indicators: true //是否显示滚动条
});
var vm = new Vue({
	el: '#app',
	data: {
		wdsl: 0,
		ydsl: 0,
		weidus: [],
		yidus: []
	},
	methods: {
		ydbtn: function(myid) {
			$.ajax({
				type: "PUT",
				url: dataDomain + "/dc/alarm/read",
				data: {
					id: myid
				},
				success: function(req) {
					if(req.status == 'ok') {
						reload();
					}
				}
			});
		}
	}
});
$(function() {
	mui.previewImage();

	$("#bk").on("tap", function() {
		var dcWin = plus.webview.getWebviewById('dc');
		if(typeof(dcWin) != 'undefined' && dcWin != '' && null != dcWin) {
			dcWin.evalJS("reload()");
		}
		mui.openWindow({
			url: "dc.html",
			id: "dc",
			createNew: false,
			show: {
				aniShow: "none"
			}
		});
		$(".mui-bar-tab a").removeClass("mui-active");
		$("#bj").addClass("mui-active");
		return false;
	});
	$("#bj").on("tap", function() {
		var bjWin = plus.webview.getWebviewById('bj');
		if(typeof(bjWin) != 'undefined' && bjWin != '' && null!=bjWin) {
			bjWin.evalJS("reload()");
		}
		mui.openWindow({
			url: "index.html",
			id: "bj",
			createNew: false,
			show: {
				aniShow: "none"
			}
		});
		$(".mui-bar-tab a").removeClass("mui-active");
		$("#bj").addClass("mui-active");
		return false;
	});
	$("#wd").on("tap", function() {
		mui.openWindow({
			url: "setting.html",
			id: "wd",
			createNew: false,
			show: {
				aniShow: "none"
			}
		});
		$(".mui-bar-tab a").removeClass("mui-active");
		$("#bj").addClass("mui-active");
		return false;
	});
	reload();
});

function reload() {
	weidufun();
	yidufun();
}

function weidufun() {
	$.get(dataDomain + "/dc/alarms?isRead=0", function(req) {
		if(req == "") {
			return;
		}
		if(req.data.length > 0) {
			vm.wdsl = req.data.length;
			vm.weidus = req.data;
		} else {
			vm.wdsl = 0;
			vm.weidus = [];
		}
	});
}

function yidufun() {
	$.get(dataDomain + "/dc/alarms?isRead=1", function(req) {
		if(req == "") {
			return;
		}
		if(req.data.length > 0) {
			vm.ydsl = req.data.length;
			vm.yidus = req.data;
		} else {
			vm.ydsl = 0;
			vm.yidus = [];
		}
	});
}