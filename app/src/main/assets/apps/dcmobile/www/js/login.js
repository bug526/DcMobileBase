$("#divtop").hide();
$("#headertop").hide();

mui.init();

mui.plusReady(function() {
	function autologin() {
		var account = localStorage.getItem("account");
		var passowrd = localStorage.getItem("password");
		$.post(dataDomain + "/common/adminuser/login", {
			usern: account,
			password: passowrd
		}, function(req) {
			if(req.status == 'ok') {
				loginSuccessFun();
				localStorage.setItem("adminuser", JSON.stringify(req.adminuser));
				mui.openWindow({
					url: "index.html",
					id: "index",
					createNew: false
				})
			} else {
				$("#divtop").show();
				$("#headertop").show();
			}

		}).error(function() {
			mui.alert("服务器网络异常，请稍候再试。");
		});
	};
	autologin();
	var account = localStorage.getItem("account");
	var passowrd = localStorage.getItem("password");
	if(account == null || passowrd == null) {
		$("#divtop").show();
		$("#headertop").show();
	}
	//				 
	mui(".mui-content-padded").on("tap", "#login", function() {
		var account = $("#account").val();
		var password = $("#password").val();
		if(account == "") {
			mui.alert("请输入帐号");
			return false;
		}
		if(password == "") {
			mui.alert("请输入密码");
			return false;
		}
		$.post(dataDomain + "/common/adminuser/login", {
			usern: account,
			password: password
		}, function(req) {
			if(req.status == 'ok') {
				localStorage.setItem("password", password);
				localStorage.setItem("account", account);
				localStorage.setItem("adminuser", JSON.stringify(req.adminuser));
				loginSuccessFun();
				mui.openWindow({
					url: "index.html",
					id: "index",
					createNew: false
				})
			} else {
				mui.toast("用户名或密码错误");
			}
		}).error(function() {
			mui.alert("服务器网络异常，请稍候再试。");
		});
	});
});

function loginSuccessFun() {
	var adminuser = JSON.parse(localStorage.adminuser);
	$.ajax({
		type: "get",
		url: dataDomain + "/common/code/getCodeList",
		async: false,
		success: function(req) {
			plus.DDPush.start(req.ddpush_ip, req.ddpush_upd_port, adminuser.id, _callback);
		}
	});
}

function _callback(obj) {
	var op = obj.op;
	if(op == 1) { //来消息
		if(obj.audit_type == 'alarm') {
			var bjWin = plus.webview.getWebviewById('bj');
			if(typeof(bjWin) != 'undefined' && bjWin != '' && null != bjWin) {
				bjWin.evalJS("reload()");
			}
		} else {
			var dcWin = plus.webview.getWebviewById('dc');
			if(typeof(dcWin) != 'undefined' && dcWin != '' && null != dcWin) {
				dcWin.evalJS("reload()");
			}
		}
	} else { //点开消息
		if(obj.audit_type == 'alarm') {
			mui.openWindow({
				url: "index.html",
				id: "bj",
				createNew: false,
				show: {
					aniShow: "none"
				}
			});
		} else {
			mui.openWindow({
				url: "dc.html",
				id: "dc",
				createNew: false,
				show: {
					aniShow: "none"
				}
			});
		}
	}
	plus.DDPush.refresh(_callback);
}