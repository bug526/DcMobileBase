mui.init({
	swipeBack: true
});
mui('#scroll1').scroll({
	indicators: true //是否显示滚动条
});
mui('#scroll2').scroll({
	indicators: true //是否显示滚动条
});
var adminuser = JSON.parse(localStorage.adminuser);
var weibukongall = new Array();
var weibukongsear = 0;
var daishenghesall = new Array();
var daishenghessear = 0;
var yibukongsall = new Array();
var yibukongssear = 0;
var vm = new Vue({
	el: '#app',
	data: {
		weibukongs: [],
		daishenghes: [],
		yibukongs: [],
		adminuser: adminuser
	},
	methods: {
		wbkfun: function(myid) {
			mui.openWindow({
				url: "add_dc.html",
				id: "edit_dc",
				createNew: false,
				extras: {
					mainid: myid
				}
			})
		},
		dshfun: function(myid) {
			mui.openWindow({
				url: "add_dc.html",
				id: "edit_dc",
				createNew: false,
				extras: {
					mainid: myid
				}
			})
		},
		ybkfun: function(myid) {
			mui.openWindow({
				url: "add_dc.html",
				id: "edit_dc",
				createNew: false,
				extras: {
					mainid: myid
				}
			});
		}
	}
});

$(function() {
	$("#tjbk").on("tap", function() {
		mui.openWindow({
			id: "add_dc",
			url: "add_dc.html",
			createNew: false
		});
	});

	document.getElementById("weibukongsearch").addEventListener('input', function() {

		if(weibukongsear == 0) {
			weibukongall = vm.weibukongs;
		} else {
			vm.weibukongs = weibukongall;
		}
		weibukongsear++;

		var weibukongsrealName = new Array();

		if(this.value != "") {
			for(var i = 0; i < vm.weibukongs.length; i++) {
				if(vm.weibukongs[i].realName.indexOf(this.value) > -1 || vm.weibukongs[i].cardNo.indexOf(this.value) > -1) {
					weibukongsrealName.push(vm.weibukongs[i]);
				}
			}
			vm.weibukongs = weibukongsrealName;
		}

	});
	document.getElementById("daishenghessearch").addEventListener('input', function() {

		if(daishenghessear == 0) {
			daishenghesall = vm.daishenghes;
		} else {
			vm.daishenghes = daishenghesall;
		}
		daishenghessear++;

		var realName = new Array();

		if(this.value != "") {
			for(var i = 0; i < vm.daishenghes.length; i++) {
				if(vm.daishenghes[i].realName.indexOf(this.value) > -1 || vm.daishenghes[i].cardNo.indexOf(this.value) > -1) {
					realName.push(vm.daishenghes[i]);
				}
			}
			vm.daishenghes = realName;
		}

	});
	document.getElementById("yibukongssearch").addEventListener('input', function() {

		if(yibukongssear == 0) {
			yibukongsall = vm.yibukongs;
		} else {
			vm.yibukongs = yibukongsall;
		}
		yibukongssear++;

		var realName = new Array();

		if(this.value != "") {
			for(var i = 0; i < vm.yibukongs.length; i++) {
				if(vm.yibukongs[i].realName.indexOf(this.value) > -1 || vm.yibukongs[i].cardNo.indexOf(this.value) > -1) {
					realName.push(vm.yibukongs[i]);
				}
			}
			vm.yibukongs = realName;
		}

	});
	//	$("#weibukongsearch").addEventListener('input', function() {
	//		if(weibukongsear == 0) {
	//			weibukongall = vm.weibukongs;
	//		} else {
	//			vm.weibukongs = weibukongall;
	//		}
	//		weibukongsear++;
	//
	//		var weibukongsrealName = new Array();
	//
	//		if($("#weibukongsearch").val() != "") {
	//			for(var i = 0; i < vm.weibukongs.length; i++) {
	//				if(vm.weibukongs[i].realName.indexOf($("#textname").val()) > -1) {
	//					weibukongsrealName.push(vm.weibukongs[i]);
	//				}
	//			}
	//			vm.weibukongs = weibukongsrealName;
	//		}
	//		if($("#weibukongsearch").val() != "") {
	//			for(var i = 0; i < vm.weibukongs.length; i++) {
	//				if(vm.weibukongs[i].cardNo.indexOf($("#textcardno").val()) > -1) {
	//					weibukongsrealName.push(vm.weibukongs[i]);
	//				}
	//			}
	//			vm.weibukongs = weibukongsrealName;
	//		}
	//	});
	$("#scbk").on("tap", function() {
		var arr = $(".mui-checkbox input[type='checkbox']:checkbox:checked");
		if(arr.length <= 0) {
			mui.toast("未选择删除项");
			return false;
		}
		var idstr = "";
		arr.each(function() {
			var myid = $(this).val();
			idstr += myid + ",";
		});
		if(idstr != '') {
			idstr = idstr.substring(0, idstr.length - 1);
		}
		$.ajax({
			type: "DELETE",
			url: dataDomain + "/dc/info?ids=" + idstr,
			async: true,
			success: function(req) {
				if(req.status == 'ok') {
					mui.toast("删除成功");
					reload();
				} else {
					mui.toast("删除失败，请稍候再试");
				}
			}
		});
		return false;
	});
	var userPicker = new mui.PopPicker();
	$.get(dataDomain + "/common/auditusers", function(req) {
		userPicker.setData(req);
	});
	$("#tjsh").on("tap", function() {
		var arr = $(".mui-checkbox input[type='checkbox']:checkbox:checked");
		if(arr.length <= 0) {
			mui.toast("未选择提交项");
			return false;
		}

		var idstr = "";
		arr.each(function() {
			var myid = $(this).val();
			idstr += myid + ",";
		});

		localStorage.setItem("idstr", idstr);
		mui.openWindow({
			id: "tjsh_dc",
			url: "tjsh_dc.html?idstr=" + idstr,
			show: {
				aniShow: "none"
			}
		})
	});

	$("#bk").on("tap", function() {
		mui.openWindow({
			url: "dc.html",
			id: "dc",
			createNew: false
		});
		$(".mui-bar-tab a").removeClass("mui-active");
		$("#bk").addClass("mui-active");
		return false;
	});
	$("#bj").on("tap", function() {
		mui.openWindow({
			url: "index.html",
			id: "bj",
			createNew: false
		});
		$(".mui-bar-tab a").removeClass("mui-active");
		$("#bk").addClass("mui-active");
		return false;
	});
	$("#wd").on("tap", function() {
		mui.openWindow({
			url: "setting.html",
			id: "wd",
			createNew: false
		});
		$(".mui-bar-tab a").removeClass("mui-active");
		$("#bk").addClass("mui-active");
		return false;
	});
	reload();
	//普通民警
	if(adminuser.roleNo == '1001') {
		$("#tjsh").parent().show();
	} else if(adminuser.roleNo == '1002') { //中层领导
		$("#tjbk1").parent().show();
	} else if(adminuser.roleNo == '1003') { //高层领导
		$("#tjbk1").parent().show();
		$("#daishenhe").hide();
	}
	$("#tjbk1").on("tap", function() {
		var arr = $(".mui-checkbox input[type='checkbox']:checkbox:checked");
		if(arr.length <= 0) {
			mui.toast("未选择提交项");
			return false;
		}
		var idstr = "";
		arr.each(function() {
			var myid = $(this).val();
			idstr += myid + ",";
		});
		$.post(dataDomain + "/dc/info/commit", {
			ids: idstr
		}, function(req) {
			if(req.status == 'ok') {
				mui.toast("提交布控成功");
				reload();
			} else {
				mui.toast("提交失败，请稍候再试");
			}
		});
	});
	$("#segmentedControl a").on("tap", function() {
		var txt = $.trim($(this).text());
		if(txt == '未布控') {
			$("#rightbtn").show();
			$("#rightbtn").attr("href", "#topPopover");
			$(".mui-popover").css("height", "150px");
		} else if(txt == '待审核') {
			if(adminuser.roleNo == '1002') {
				$("#rightbtn").attr("href", "#topPopoverShbk");
				$(".mui-popover").css("height", "100px");
			} else {
				$("#rightbtn").hide();
			}
		} else if(txt == '已布控') {
			$("#rightbtn").show();
			$("#rightbtn").attr("href", "#topPopoverQxbk");
			$(".mui-popover").css("height", "50px");
		}
	});
	$("#quxiaobukong").on("tap", function() {
		var arr = $(".mui-checkbox input[type='checkbox']:checkbox:checked");
		if(arr.length <= 0) {
			mui.toast("未选择提交项");
			return false;
		}

		var idstr = "";

		arr.each(function() {
			var myid = $(this).val();
			idstr += myid + ",";
		});
		//如果是民警
		if(adminuser.roleNo == '1001') {
			var arr = $(".mui-checkbox input[type='checkbox']:checkbox:checked");
			var idstr = "";
			arr.each(function() {
				var myid = $(this).val();
				idstr += myid + ",";
			});

			localStorage.setItem("idstr", idstr);
			mui.openWindow({
				id: "quxiao_dc",
				url: "quxiao_dc.html",
				createNew: false
			})
		} else {
			var arr = $(".mui-checkbox input[type='checkbox']:checkbox:checked");
			var idstr = "";
			arr.each(function() {
				var myid = $(this).val();
				idstr += myid + ",";
			});

			localStorage.setItem("idstr", idstr);
			mui.openWindow({
				id: "quxiao_nosh_dc",
				url: "quxiao_nosh_dc.html",
				createNew: true,
				show: {
					aniShow: "none"
				}
			})
		}
	});
	$("#tongguo").on("tap", function() {
		var arr = $(".mui-checkbox input[type='checkbox']:checkbox:checked");
		if(arr.length <= 0) {
			mui.toast("未选择提交项");
			return false;
		}

		var idstr = "";

		arr.each(function() {
			var myid = $(this).val();
			idstr += myid + ",";
		});
		$.post(dataDomain + "/dc/info/pass", {
			ids: idstr
		}, function(req) {
			if(req.status == 'ok') {
				mui.toast("审批已成功提交");
				reload();
			} else {
				mui.toast("提交失败，请稍候再试");
			}
		});
	});
	$("#jujue").on("tap", function() {
		var arr = $(".mui-checkbox input[type='checkbox']:checkbox:checked");
		if(arr.length <= 0) {
			mui.toast("未选择提交项");
			return false;
		}
		var idstr = "";
		arr.each(function() {
			var myid = $(this).val();
			idstr += myid + ",";
		});
		localStorage.setItem("idstr", idstr);
		mui.openWindow({
			id: "jujue_dc",
			url: "jujue_dc.html",
			createNew: false
		});
	});
});

function reload() {
	$("input:checkbox").removeAttr("checked");
	weibkfun();
	dashfun();
	yibkfun();
}

function weibkfun() {
	$.get(dataDomain + "/dc/infos?state=0", function(req) {
		if(req.data.length > 0) {
			vm.weibukongs = req.data;
		} else {
			vm.weibukongs = [];
		}
	});
}

function dashfun() {
	$.get(dataDomain + "/dc/infos?state=1", function(req) {
		if(req.data.length > 0) {
			vm.daishenghes = req.data;
		} else {
			vm.daishenghes = [];
		}
	});
}

function yibkfun() {
	$.get(dataDomain + "/dc/infos?state=2", function(req) {
		if(req.data.length > 0) {
			vm.yibukongs = req.data;
		} else {
			vm.yibukongs = [];
		}
	});
}