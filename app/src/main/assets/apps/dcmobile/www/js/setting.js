mui.init();
//初始化单页view
var viewApi = mui('#app').view({
	defaultPage: '#setting'
});
var adminuser = JSON.parse(localStorage.adminuser);

mui.plusReady(function() {

});

var view = viewApi.view;
(function($) {
	//处理view的后退与webview后退
	var oldBack = $.back;
	$.back = function() {
		if(viewApi.canBack()) { //如果view可以后退，则执行view的后退
			viewApi.back();
		} else { //执行webview后退
			oldBack();
		}
	};
})(mui);

$(function() {
	$("#log").on("tap", function() {
		mui.openWindow({
			url: "log.html",
			id: "log",
			createNew: false
		});
	});
	$("#bk").on("tap", function() {
		mui.openWindow({
			url: "dc.html",
			id: "dc",
			createNew: false
		});
		$(".mui-bar-tab a").removeClass("mui-active");
		$("#wd").addClass("mui-active");
		return false;
	});
	$("#bj").on("tap", function() {
		mui.openWindow({
			url: "index.html",
			id: "bj",
			createNew: false
		});
		$(".mui-bar-tab a").removeClass("mui-active");
		$("#wd").addClass("mui-active");
		return false;
	});
	$("#wd").on("tap", function() {
		mui.openWindow({
			url: "setting.html",
			id: "wd",
			createNew: false
		});
		$(".mui-bar-tab a").removeClass("mui-active");
		$("#wd").addClass("mui-active");
		return false;
	});
	$("#log").on("tap", function() {

		mui.openWindow({
			url: "log.html",
			id: "log",
			createNew: false
		});

		return false;
	});
	$("#tcdl").click(function() {
		mui.confirm("确定退出系统吗？", "提示", function(e) {
			if(e.index == 1) {
				localStorage.clear();
				 
				$.post(dataDomain + "/common/adminuser/exit", {}, function(req) {
					if(req.status == 'ok') {
						var curr = plus.webview.currentWebview();
                                var wvs = plus.webview.all();
                                for (var i = 0, len = wvs.length; i < len; i++) {
                                    //关闭除setting页面外的其他页面
                                    if (wvs[i].getURL() == curr.getURL())
                                        continue;
                                    plus.webview.close(wvs[i]);
                                }
                                //打开login页面后再关闭setting页面
                                plus.webview.open('login.html');
                                curr.close();
//						mui.openWindow({
//							url: "login.html",
//							id: "login",
//							createNew: false
//						})
					} else {
						mui.toast("请求失败，请稍候再试。");
					}
				});
			}
		});
	});
	$("#realname").html(adminuser.realName);
	$("#jinghao").html(adminuser.policeNo);
	$("#gr_xm").val(adminuser.realName);
	$("#gr_sex").val(adminuser.sex);
	$("#gr_jh").val(adminuser.policeNo);
	$("#gr_sj").val(adminuser.phone);
	$("#grxx").on("tap", function() {
		$.ajax({
			type: "PUT",
			url: dataDomain + "/common/adminuser/" + adminuser.id,
			data: {
				realName: $("#gr_xm").val(),
				sex: $("#gr_sex").val(),
				phone: $("#gr_sj").val(),
				policeNo: $("#gr_jh").val()
			},
			success: function(req) {
				if(req.status == 'ok') {
					mui.toast("修改成功。");
				} else {
					mui.toast("修改失败，请稍候再试。");
				}
			}
		});
	});
	$("#xgmm").on("tap", function() {
		var oldpwd = $("#oldpwd").val();
		var newpwd = $("#newpwd").val();
		var rnewpwd = $("#rnewpwd").val();
		if(oldpwd == '') {
			mui.alert("请输入旧密码");
			return false;
		}
		if(newpwd == '') {
			mui.alert("请输入新密码");
			return false;
		}
		if(rnewpwd == '') {
			mui.alert("请输入重复新密码");
			return false;
		}
		if(newpwd != rnewpwd) {
			mui.alert("两次密码输入不一致");
			return false;
		}
		$.ajax({
			type: "PUT",
			url: dataDomain + "/common/adminuser/updpwd",
			data: {
				id: adminuser.id,
				oldpwd: $("#oldpwd").val(),
				newpwd: $("#newpwd").val()
			},
			async: true,
			success: function(req) {
				if(req.status == 'ok') {
					mui.toast("密码修改成功");
					$.post(dataDomain + "/common/adminuser/exit", {}, function(req) {
						if(req.status == 'ok') {
							mui.openWindow({
								url: "login.html",
								id: "login",
								createNew: false
							})
						} else {
							mui.toast("请求失败，请稍候再试。");
						}
					});
				} else if(req.status == 'olderror') {
					mui.toast("旧密码输入错误，修改失败");
				} else {
					mui.toast("请求失败，请稍候再试");
				}
			}
		});
	});
});