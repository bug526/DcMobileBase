function addDDPushPlugin() {
 var _BARCODE = 'DDPush',
  B = window.plus.bridge;
 var DDPush = {
  start: function(_ip, _port, _id, _callBack) {

      var _callbackId =B.callbackId(_callBack,null);
   return B.exec(_BARCODE, "start", [_callbackId,_ip, _port, _id]);
  },
  refresh: function(_callBack) {
              var _callbackId =B.callbackId(_callBack,null);
           return B.exec(_BARCODE, "refresh", [_callbackId]);
          },
  stop: function() {
   return B.exec(_BARCODE, "stop", []);
  }
 };
 window.plus.DDPush = DDPush;
}

if(window.plus) {
 addDDPushPlugin();
} else {
 document.addEventListener('plusready', addDDPushPlugin, false);
}